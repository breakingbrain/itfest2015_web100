from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

from web100_django.server.views import ServerView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'web100_django.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # url(r'^admin/', include(admin.site.urls)),
    url(r'^$', ServerView.as_view(), name='home'),
)
