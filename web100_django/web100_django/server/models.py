from django.db import models

# Create your models here.


class ClientModel(models.Model):
    public_key = models.CharField(max_length=32)
    password = models.CharField(max_length=50, default='Q0VTvQokuq')
    user_agent = models.CharField(max_length=100, default="Tea-Client")