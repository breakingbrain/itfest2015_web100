# -*- coding: utf-8 -*-

import random

from django.shortcuts import render, render_to_response, RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic.base import View
# Create your views here.

from web100_django.server.models import ClientModel
from django.utils.html import escape


from hashlib import md5

import string

# class UserPassesTestMixin(object):
#     '''
#     Класс для отображения данных только
#     зарегистрированным в системе пользователям
#     '''
#     request = None
#
#     def user_passes_test(self, request):
#         if :
#
#         return False
#
#     def user_failed_test(self):
#         return HttpResponseRedirect('/start/')
#
#     def dispatch(self, request, *args, **kwargs):
#         self.request = request
#         if not self.user_passes_test(request):
#             return self.user_failed_test()
#         return super(UserPassesTestMixin, self).dispatch(request, *args, **kwargs)


class ServerView(View):
    n = 3503447802937491650149976605364596738766983598118786883888746474591603613031449913602376087094773488936670665044152266013953743257099608011477520148677896157941784099891804604890260493682415232323851469912407331213986841680534595722438687

    tea = {
        'black': 'somevalue',
        'green': 'somevalue',
        'flag': 'T3@1sS0Go()D'
    }

    def proc_ident(self, request):
        answer = {}
        if 'round' not in request.session:

            request.session['round'] = 30
            request.session['part'] = 1
        if request.session['part'] == 1:
            if 'x' not in request.GET:
                return False                                            # Fail
            request.session['x'] = request.GET['x']
            e = random.randint(0, 1)        # B -> A
            request.session['part'] = 2
            request.session['e'] = e
            answer = {
                'e': e
            }
        elif request.session['part'] == 2:
            if 'y' not in request.GET:                                  # Fail
                return False
            y = int(request.GET['y'])
            x = int(request.session['x'])
            e = int(request.session['e'])
            V = int(request.session['public_key'])
            #public key
            if y == 0:
                return False
            if (y**2) % self.n != (x * V**e) % self.n:
                return False
            request.session['part'] = 1
            request.session['round'] -= 1
            if request.session['round'] <= 0:
                request.session['connection'] = 2
                answer = {
                    'next_step': True
                }
            else:
                answer = {
                    'next_round': True
                }
        return answer

    def login(self, request):                           # логин в систему
        if 'password' not in request.GET:
            return False

        password = escape(request.GET['password'])
        user_agent = request.META['HTTP_USER_AGENT']

        public_key = request.session['public_key']
        query_str = string.Template('SELECT * FROM server_clientmodel WHERE public_key=MD5("%s") and password="%r" and '
                                    ' user_agent="$user_agent"')
        query_str = query_str.safe_substitute({'user_agent': user_agent})
        res = ClientModel.objects.raw(query_str, [int(public_key), password])[:]
        if len(res) != 1:
            return False
        request.session['connection'] = 3
        return True

    def make_tea(self, request):
        if 'make' not in request.GET:
            return False

        tea = request.GET['make']

        return self.tea.get(tea, 'I haven\'t this is sort of tea =(')

    @staticmethod
    def clear_session(request):
        try:
            del request.session['connection']
        except:
            pass
        try:
            del request.session['public_key']
        except:
            pass
        try:
            del request.session['round']
        except:
            pass
        try:
           del request.session['part']
        except:
            pass


    def client_substitution(self, request):
        if request.session['public_key'] != request.GET['public_key']:
            self.clear_session(request)
            return True
        else:
            return False

    def get(self, request):
        if request.META['HTTP_USER_AGENT'].find('Tea-Client') != 0:  # проверка вхождения, так как могут быть
            return HttpResponse(status=418)                          # разные версии клиентов (спец. косяк)

        if 'connection' in request.session:
            if request.session['connection'] == 1:                  # процесс доказательства идентичности
                answer = self.proc_ident(request)
                if not answer:
                    self.clear_session(request)
                    return HttpResponse("Fail", status=403)
                return render_to_response("show_me_process", answer, context_instance=RequestContext(request,
                                                                                                     processors=[]))
            elif request.session['connection'] == 2:                # процесс логина
                if not self.login(request):
                    self.clear_session(request)
                    return HttpResponse("Fail", status=403)
                return HttpResponse("Success")
            elif request.session['connection'] == 3:                # готовка чая
                answer = self.make_tea(request)
                if not answer:
                    self.clear_session(request)
                    return HttpResponse("Fail", status=403)
                return HttpResponse(answer)
        else:
            if 'public_key' in request.GET:
                public_key = request.GET['public_key']
                h = md5(public_key).hexdigest()
                try:                                                    # создаем отсутствующего пользователя
                    client = ClientModel.objects.get(public_key=h)
                except ClientModel.DoesNotExist:
                    client = ClientModel(public_key=h)
                    client.save()

                request.session['connection'] = 1
                request.session['public_key'] = public_key
                return HttpResponse("show_me", status=200)
            else:
                return HttpResponse('fail', status=403)
        return HttpResponse('fail', status=403)


    def post(self, request):
        return