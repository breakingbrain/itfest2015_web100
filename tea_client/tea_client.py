# -*- coding: utf-8 -*-

import urllib
import urlparse
import random

from parser import WebAgentSim
from prime_num import AlgEvklid, Zpow




class TeaClient:

    def __init__(self):
        self.address = 'localhost:8000'
        self.n = 3503447802937491650149976605364596738766983598118786883888746474591603613031449913602376087094773488936670665044152266013953743257099608011477520148677896157941784099891804604890260493682415232323851469912407331213986841680534595722438687
        self.keys = self.read_keys()
        self.agent = WebAgentSim()

    def read_keys(self):
        try:
            fin = open("keys", 'r')
        except IOError:
            keys = self.generation_keys()
            fout = open("keys", 'w')
            fout.write(str(keys['public_key']) + "\n" + str(keys['private_key']))
            fout.close()
        else:
            v = int(fin.readline())
            s = int(fin.readline())
            fin.close()
            keys = {
                'public_key': v,
                'private_key': s
            }
        return keys

    def generation_keys(self):
        s = random.randint(1, self.n-1)          # генерация взаимного простого с n user'а A
        while AlgEvklid(self.n, s) != 1:
            s = random.randint(1, self.n-1)
        v = Zpow(s, 2, self.n)                   # открытый ключ А
        return {
            'public_key': v,
            'private_key': s
        }

    def send_info(self, query):

        params = ['http', self.address, '/', '', '', '']
        params[4] = urllib.urlencode(query)
        url = urlparse.urlunparse(params)
        html = self.agent.get_page(url)
        return html

    def connection(self):
        info = {'public_key': self.keys['public_key']}
        answer = self.send_info(info)
        if answer == 'show_me':
            return True
        return False

    def identification(self):

        for i in xrange(30):
            r = random.randint(1, self.n-1)
            x = Zpow(r, 2, self.n)
            res = self.send_info({'x': x})
            try:
                e = int(res)
            except:
                return False
            if e == 0:
                y = r
            else:
                y = r*self.keys['private_key'] % self.n

            res = self.send_info({'y': y})
        return True

    def login(self):
        password = raw_input("Enter password for tea-server: ")
        answer = self.send_info({'password': password})
        if answer == "Success":
            return True
        return False

    def make_tea(self):
        tea = raw_input("What kind of tea do you want? Enter q for exit: ")
        answer = self.send_info({'make': tea})
        print answer


def main():
    print "Welcome to Tea-Client v.0.1!"
    tea_client = TeaClient()
    print "I'm trying to connect to the Tea-server..."
    if not tea_client.connection():
        print "Connection error!"
        return
    print "The connection is established"
    print "Identification..."
    if not tea_client.identification():
        print "Identification error!"
        return
    print "Identification is passed!"
    print "Please, login in the system"
    if not tea_client.login():
        print "Login Error!"
        return
    tea_client.make_tea()
    print "Enjoy tea!"
    return

if __name__ == '__main__':
    main()