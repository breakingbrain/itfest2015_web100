import pycurl
from StringIO import StringIO


class WebAgentSim:
    def __init__(self):
        self.curl = pycurl.Curl()

    def get_page(self, url, *args, **kargs):
        self.curl.setopt(pycurl.URL, url)
        self.curl.bodyio = StringIO()
        self.curl.setopt(pycurl.WRITEFUNCTION, self.curl.bodyio.write)
        self.curl.get_body = self.curl.bodyio.getvalue
        self.curl.headio = StringIO()
        self.curl.setopt(pycurl.HEADERFUNCTION, self.curl.headio.write)
        self.curl.get_head = self.curl.headio.getvalue

        self.curl.setopt(pycurl.FOLLOWLOCATION, 1)
        self.curl.setopt(pycurl.MAXREDIRS, 5)
        self.curl.setopt(pycurl.CONNECTTIMEOUT, 60)
        self.curl.setopt(pycurl.TIMEOUT, 120)
        self.curl.setopt(pycurl.NOSIGNAL, 1)
        self.curl.setopt(pycurl.USERAGENT, 'Tea-Client')
        self.curl.setopt(pycurl.COOKIELIST, "mycookie")
        httpheader = [
            'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language: ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3',
            'Accept-Charset:utf-8;q=0.7,*;q=0.5',
            'Connection: keep-alive',
            ]
        self.curl.setopt(pycurl.HTTPHEADER, httpheader)
        self.curl.perform()
        if self.curl.getinfo(pycurl.HTTP_CODE) != 200:
            raise Exception('HTTP code is %s' % self.curl.getinfo(pycurl.HTTP_CODE))
    
        return self.curl.get_body()


