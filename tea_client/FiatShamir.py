# -*- coding: utf-8 -*-

__author__ = 'tiso'

import random
from prime_num import generate_prime, AlgEvklid, Zpow


def FiatShamitProtocol():
    p = None
    q = None
    while p is None:
        p = generate_prime(271345838280909634783840377047987070130610929692119657962127, 100)
    print "p: " + str(p)
    while q is None:
        q = generate_prime(271345838280909634783840377047987070130610929692119657962127, 100)
    print "q: " + str(q)
    n = p*q                             # Доверенный центр Трент публикает
    print "n: " + str(n)
    s = random.randint(1, n-1)          # генерация взаимного простого с n user'а A
    print "s " + str(s)
    while AlgEvklid(n, s) != 1:
        s = random.randint(1, n-1)


    V = Zpow(s, 2, n)                   # открытый ключ А
    print "V " + str(V)
    t = 40
    # аккредитация
    for i in range(1, t):
        r = random.randint(1, n-1)
        x = Zpow(r, 2, n)               # стороне B
        e = random.randint(0, 1)        # B -> A
        if e == 0:
            y = r
        else:
            y = r*s % n                 # A -> B

        if y == 0:
            return False

        if (y**2) %n != (x * V**e) % n:
            return False

    return True


def main():
    print FiatShamitProtocol()
    return


if __name__ == "__main__":
    main()